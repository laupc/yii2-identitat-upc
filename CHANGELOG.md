1.1.0 / 2019-07-22
==================

  * Definir valors per defecte UPC

1.0.1 / 2019-05-27
==================

  * Permetre diferents usuaris mitjançant $I->amLoggedInAs en cas de testing

1.0.0 / 2019-05-27
==================

  * Mòdul Yii2 per autenticar mitjançant Single Sign On (CAS).
