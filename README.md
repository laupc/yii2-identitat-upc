yii2-identitat-upc
==================

yii2-identitat-upc implementa la validació CAS en el servidor de la UPC.

Requisits
---------

yii2-identitat-upc necessita que el servidor on s'executi tingui accés al servidor CAS de la UPC.

Instal·lació
------------

La manera recomenada d'instal·lar aquesta extensió és mitjançant [composer](http://getcomposer.org/download/).

Cal que afegeixis

```json
"upc/yii2-identitat-upc": "*",
```

a la secció `require` del fitxer `composer.json` de la teva aplicació.

```bash
composer install
```

Això crearà el directori `upc\yii2-identitat-upc` dins del directori `vendor` de la teva aplicació.

Utilització
-----------

Configura el component `user`

```php
'user' => [
    'class' => 'upc\identitat\User',
    'identityClass' => 'app\models\User',
    'serverHostname' => 'sso.upc.edu',
    'serverUri' => '/CAS',
    'serverClientUri' => 'https://application.upc.edu',
    'serverVersion' => '2.0',
    'serverPort' => 443,
    'verbose' => false, // Defaults to false
    // 'testUser' => 'username' // Defaults to false
],
```

a la secció `components` del fitxer de coniguració de l'aplicació. El fitxer de configuració de l'aplicació es troba normalment a `app\config`.

A continuació cal modificar les accions `actionLogin` i `actionLogout` (normalment localitzades a `SiteController`).

```php
public function actionLogin()
{
    Yii::$app->user->login(Yii::$app->user->authenticate());
    return $this->redirect(['site/index']);
}

public function actionLogout()
{
    Yii::$app->user->logout();
    return $this->redirect(['site/index']);
}
```
Finalment ens hem d'assegurar que la classe  introduïda al camp  `identityClass` de `user` implementi el mètode `findByUsername()` i  l'interfaç `IdentityInterace`.

```php
    /**
     * Finds an identity by the given username.
     *
     * @param string $username the username to be looked for
     * @return IdentityInterface|null the identity object that matches the given username.
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

   /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['username' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
   {
       throw new NotSupportedException("findIdentityByAccessToken not implemented");
   }

   /**
    * @inheritdoc
    */
   public function getId()
   {
       return $this->username;
   }

   /**
    * @inheritdoc
    */
   public function getAuthKey()
   {
       throw new NotSupportedException("getAuthKey not implemented");
   }

   /**
    * @inheritdoc
    */
   public function validateAuthKey($authKey)
   {
       throw new NotSupportedException("validateAuthKey not implemented");
   }
```


Llicència
---------

Copyright (C) 2015-2016 Universitat Politècnica de Catalunya - UPC BarcelonaTech - www.upc.edu

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
